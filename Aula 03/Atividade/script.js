class RegistroVendas {
    constructor() {
      this.vendas = [];
    }
  
    adicionarVenda(amount) {
      this.vendas.push(amount);
    }
  
    maiorVenda() {
      return Math.max(...this.vendas);
    }
  
   mediaVenda() {
      const sum = this.vendas.reduce((acc, cur) => acc + cur, 0);
      return sum / this.vendas.length || 0;
    }
  
    gerarHTML() {
      let html = '<ul>';
      this.vendas.forEach(sale => {
        html += `<li>${sale}</li>`;
      });
      html += '</ul>';
      return html;
    }
  }
  
  const registroVendas = new RegistroVendas();
  const form = document.getElementById('formulario');
  const resultadoDiv = document.getElementById('resultado');
  
  form.addEventListener('submit', function(event) {
    event.preventDefault();
    const venda = parseFloat(document.getElementById('venda').value);
    registroVendas.adicionarVenda(venda);
  
    const maior = registroVendas.maiorVenda();
    const media = registroVendas.mediaVenda();
    const gerar = registroVendas.gerarHTML();
  
    resultadoDiv.innerHTML = `
      <p>Maior venda efetuada: ${maior}</p>
      <p>Média das vendas efetuadas: ${media}</p>
      <p>Lista das vendas efetuadas:</p>
      ${gerar}
    `;
  });