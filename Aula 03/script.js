// Classe com os Métodos
class CalculadoraMedia {
    constructor(numero1, numero2) {
        this.numero1 = numero1;
        this.numero2 = numero2;
    }

    calcularMedia() {
        return (this.numero1 + this.numero2) / 2;
    }
}

// Manipulador de evento para o formulário
document.getElementById('formulario').addEventListener('submit', function(event) {
    // Impedir o envio do formulário
    event.preventDefault();

    // Obter os valores dos campos de entrada
    const numero1 = parseFloat(document.getElementById('numero1').value);
    const numero2 = parseFloat(document.getElementById('numero2').value);

    if (isNaN(numero1) || isNaN(numero2)) {
        document.getElementById('resultado').innerText = 'Por favor, insira números válidos.';
        return;
    }

    // Criar uma instância da calculadora e calcular a média
    const calculadora = new CalculadoraMedia(numero1, numero2);
    const media = calculadora.calcularMedia();

    // Exibir o resultado na tela
    document.getElementById('resultado').innerText = `A média é: ${media}`;
});
